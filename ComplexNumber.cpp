//
// Created by Maciej Dmowski on 2019-03-10.
//

#include "ComplexNumber.h"
#include <iostream>
using namespace std;

ComplexNumber::ComplexNumber(double real, double imaginary){
    realPart = real;
    imaginaryPart = imaginary;
}


ComplexNumber ComplexNumber::operator+ (const ComplexNumber &n1) {
    ComplexNumber tmp(this->realPart + n1.realPart, this->imaginaryPart + n1.imaginaryPart);
    return tmp;
}

ComplexNumber ComplexNumber::operator+= (const ComplexNumber &n1) {
    this->realPart += n1.realPart;
    this->imaginaryPart += n1.imaginaryPart;
    return *this;
}

ComplexNumber ComplexNumber::operator- (const ComplexNumber &n1) {
    ComplexNumber tmp(this->realPart - n1.realPart, this->imaginaryPart - n1.imaginaryPart);
    return tmp;
}

ComplexNumber ComplexNumber::operator-= (const ComplexNumber &n1) {
    this->realPart -= n1.realPart;
    this->imaginaryPart -= n1.imaginaryPart;
    return *this;
}

bool ComplexNumber::operator==(const ComplexNumber &n1){
    if(( this->realPart == n1.realPart ) && ( this->imaginaryPart == n1.imaginaryPart ))
        return true;
    else
        return false;
}

bool ComplexNumber::operator!=(const ComplexNumber &n1){
    if(( this->realPart != n1.realPart ) || ( this->imaginaryPart != n1.imaginaryPart ))
        return true;
    else
        return false;
}

ostream& operator<<( ostream& s, const ComplexNumber& n1 )
{
    s <<n1.realPart<<" + "<<n1.imaginaryPart<<"i"<<endl;
    return s;
}

double ComplexNumber::getReal() {
    return this->realPart;
}
double ComplexNumber::getImaginary(){
    return this->imaginaryPart;
}

