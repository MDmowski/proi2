//
// Created by Maciej Dmowski on 2019-03-10.
//

#ifndef PROJECT2_COMPLEXNUMBER_H
#define PROJECT2_COMPLEXNUMBER_H

#include <iostream>

class ComplexNumber {
protected:
    double realPart;
    double imaginaryPart;
public:
    explicit ComplexNumber(double=0, double=0);
    double getReal();
    double getImaginary();
    ComplexNumber operator+ (ComplexNumber const &n1);
    ComplexNumber operator+= (ComplexNumber const &n1);
    ComplexNumber operator- (ComplexNumber const &n1);
    ComplexNumber operator-= (ComplexNumber const &n1);
    bool operator== (ComplexNumber const &n1);
    bool operator!= (ComplexNumber const &n1);
    friend std::ostream&operator<<(std::ostream&, const ComplexNumber&);

};


#endif //PROJECT2_COMPLEXNUMBER_H
