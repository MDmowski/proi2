//
// Created by Maciej Dmowski on 2019-03-10.
//

#include "List.h"
#include "Node.h"
using namespace std;

List::List(string name) {
    this->head = NULL;
    this->tail = NULL;
    this->listSize = 0;
    this->name=name;
}

Node* List::getHead() {
    return this->head;
}

Node* List::getTail(){
    return this->tail;
}

void List::addNodeAsTail(Node* new_node) {
    new_node->next = NULL;
    new_node->previous = NULL;

    if (this->head == NULL) {
        this->head = new_node;
        this->tail = this->head;
        this->listSize = this->listSize + 1;
    } else {
        this->tail->next = new_node;
        new_node->previous = this->tail;
        this->tail = new_node;
        this->listSize = this->listSize + 1;
    }
}

void List::addNodeAsHead(Node* new_node) {
    new_node->next = NULL;
    new_node->previous = NULL;

    if (this->head == NULL) {
        this->head = new_node;
        this->tail = this->head;
        this->listSize = this->listSize + 1;
    } else {
        this->head->previous = new_node;
        new_node->next = this->head;
        this->head = new_node;
        this->listSize = this->listSize + 1;
    }
}

int List::size(){
    int size = 0;
    Node* temp = this->head;
    while (temp) {
        ++size;
        temp = temp->next;
    }
    this->listSize = size;
    return this->listSize;
}

void List::display() const {
    cout<<"List name: "<<this->name<<endl<<"Number of elements: "<<this->listSize<<endl;
    Node* tmp = this->head;
    int counter = 1;
    while(tmp){
        cout <<counter<<". "<< tmp->getNumber();
        tmp = tmp->next;
        counter++;
    }
}

Node* List::getNode(int index) const{
    if (index == 0) {
        return this->head;
    } else if (index == this->listSize - 1) {
        return this->tail;
    } else if (index < 0 || index >= this->listSize) {
        return NULL;
    }
    if (index < this->listSize / 2) {
        Node* temp = this->head;
        int i = 0;
        while (temp) {
            if (i == index) {
                return temp;
            }
            i++;
            temp = temp->next;
        }
    } else {
        Node* temp = this->tail;
        int i = this->listSize - 1;
        while (temp) {
            if (i == index) {
                return temp;
            }
            i--;
            temp = temp->previous;
        }
    }
    return NULL;
}

ComplexNumber &List::operator[](int index) const{
    if (index == 0) {
        return this->head->getNumber();
    } else if (index == this->listSize - 1) {
        return this->tail->getNumber();
    } else if (index < 0 || index >= this->listSize) {
        exit(0);
    }
    if (index < this->listSize / 2) {
        Node* temp = this->head;
        int i = 0;
        while (temp) {
            if (i == index) {
                return temp->getNumber();
            }
            i++;
            temp = temp->next;
        }
    } else {
        Node* temp = this->tail;
        int i = this->listSize - 1;
        while (temp) {
            if (i == index) {
                return temp->getNumber();
            }
            i--;
            temp = temp->previous;
        }
    }
    exit(0);
}

void List::deleteNode(Node* node) {
    if(node == this->head && node == this->tail){
        this->head=NULL;
        this->tail=NULL;
        delete node;
        this->listSize--;
    }
    else if(node == this->head) {
        this->head = node->next;
        node->next->previous = NULL;
        delete node;
        this->listSize--;
    }
    else if(node == this->tail){
        this->tail = node->previous;
        node->previous->next = NULL;
        delete node;
        this->listSize--;
    }else {
        node->previous->next = node->next;
        node->next->previous = node->previous;
        delete node;
        this->listSize--;
    }
}

ostream& operator<<( ostream& out, const List& l1 )
{
    cout<<"List name: "<<l1.name<<endl<<"Number of elements: "<<l1.listSize<<endl;
    Node* temp = l1.head;
    int counter=1;
    while(temp){
        out <<counter<<". "<< temp->getNumber();
        temp = temp->next;
        counter++;
    }
    return out;
}

List List::operator+(const List l1){
    string name = "Sum of: " + this->name + " and " + l1.name;
    List tmpList(name);
    List second = l1;
    Node* temp = this->head;
    while(temp){
        tmpList.addLast(temp->getNumber().getReal(), temp->getNumber().getImaginary());
        temp=temp->next;
    }
    temp = second.head;
    while(temp){
        tmpList.addLast(temp->getNumber().getReal(), temp->getNumber().getImaginary());
        temp=temp->next;
    }
    return tmpList;
}

List List::operator+=(const List l1){
    List tmpList = l1;
    Node* temp = tmpList.head;
    while(temp){
        this->addLast(temp->getNumber().getReal(), temp->getNumber().getImaginary());
        temp=temp->next;
    }
    return *this;
}

void List::addLast(double real, double imaginary){
    ComplexNumber number(real, imaginary);
    Node* temp = new Node(number);
    this->addNodeAsTail(temp);
}
void List::addFirst(double real, double imaginary){
    ComplexNumber number(real, imaginary);
    Node* temp = new Node(number);
    this->addNodeAsHead(temp);
}

void List::deleteList(){
    Node* currentNode = this->head;
    while (currentNode)
    {
        Node* nextNode = currentNode->next;
        delete currentNode;
        currentNode = nextNode;
    }
}

