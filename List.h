//
// Created by Maciej Dmowski on 2019-03-10.
//

#ifndef PROJECT2_LIST_H
#define PROJECT2_LIST_H

#include "Node.h"
#include <iostream>
#include "ComplexNumber.h"
using namespace std;

class List {
protected:
    Node* head;
    Node* tail;
    int listSize;
public:
    string name;
    explicit List(string name = "Default");
    Node* getHead();
    Node* getTail();
    void addNodeAsTail(Node* new_node);
    void addNodeAsHead(Node* new_node);
    int size();
    void display() const;
    ~List()=default;
    void deleteList();
    ComplexNumber &operator[](int index) const;
    Node* getNode(int index) const;
    void deleteNode(Node* node);
    friend ostream& operator<<(ostream& out, const List& l1);
    List operator+ (const List l1);
    List operator+= (const List l1);
    void addLast(double real, double imaginary);
    void addFirst(double real, double imaginary);
};


#endif //PROJECT2_LIST_H
