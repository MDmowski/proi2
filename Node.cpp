//
// Created by Maciej Dmowski on 2019-03-10.
//

#include "Node.h"

Node::Node(ComplexNumber num, Node* prev, Node* n) {
    number = num;
    previous = prev;
    next = n;
}

ComplexNumber& Node::getNumber(){
    return this->number;
}
