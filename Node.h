//
// Created by Maciej Dmowski on 2019-03-10.
//

#ifndef PROJECT2_NODE_H
#define PROJECT2_NODE_H

#include "ComplexNumber.h"

class Node {
    ComplexNumber number;
public:
    Node* next;
    Node* previous;
    ComplexNumber& getNumber();
    explicit Node(ComplexNumber, Node* = NULL, Node* = NULL);
    ~Node()=default;
};


#endif //PROJECT2_NODE_H
