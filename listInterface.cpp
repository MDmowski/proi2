//
// Created by Maciej Dmowski on 2019-03-21.
//

#include "listInterface.h"

using namespace std;

void listInterface(vector<List> &lists) {
    cout << "Choose which list would you like to modify" << endl;
    unsigned int modify;
    while (!(cin >> modify) || modify > lists.size()) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Invalid input.  Try again: " << endl;
    }
    modify--;
    bool loop = true;
    while (loop) {
        cout << endl << "You are modifying now list: " << lists[modify].name << endl << endl;
        cout << "Choose what to do:" << endl;
        cout<< "a -> add an element\nd -> delete an element\ns -> show the list\nn -> change the list name\n+ -> sum two elements\n- -> subtract two elements\nc -> compare two elements\nb -> go back"<< endl;
        char mod;
        mod = secureInput<char>(mod);
        switch (mod) {
            case 'a': {
                double real, imaginary;
                cout << "Enter the real part of a complex number" << endl;
                real = secureInput(real);
                cout << "Enter the imaginary part of a complex number" << endl;
                imaginary = secureInput(imaginary);
                cout << "Would you like to add this number to the front or to the back of the list?"<< endl;
                cout << "f -> front\nb -> back" << endl;
                char place;
                place = secureInput(place);
                switch (place) {
                    case 'f':
                        lists[modify].addFirst(real, imaginary);
                        break;
                    case 'b':
                        lists[modify].addLast(real, imaginary);
                        break;
                    default:
                        cout << "Invalid input.  Try again: " << endl;
                        break;
                }
                break;
            }
            case 'd': {
                unsigned int size = lists[modify].size();
                if (!size) {
                    cout << "This list is empty. There's nothing to delete" << endl;
                } else {
                    cout << lists[modify];
                    cout << "Which element would you like to delete? Enter number from 1 to " << size
                         << endl;
                    unsigned int index;
                    while (!(cin >> index) || index > size) {
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        cout << "Invalid input.  Try again: " << endl;
                    }
                    lists[modify].deleteNode(lists[modify].getNode(index - 1));
                }
                break;
            }
            case 's': {
                cout << lists[modify];
                break;
            }
            case 'n': {
                string name;
                cout << "Enter a new name:" << endl;
                name = secureInput(name);
                lists[modify].name = name;
                break;
            }
            case '+': {
                unsigned int size = lists[modify].size();
                if (size < 2) {
                    cout << "You need at least 2 elements to sum, but you have now only " << size << endl;
                } else {
                    cout << "How would you like to sum elements?" << endl;
                    cout << "n -> as a new element\nw -> sum the first element with the second one"
                         << endl;
                    char sum;
                    sum = secureInput(sum);
                    switch (sum) {
                        case 'n': {
                            cout << lists[modify] << endl;
                            cout
                                    << "Enter the numbers of elements you would like to sum. They will be summed in the entered order."
                                    << endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            ComplexNumber tmp = lists[modify].getNode(num1 - 1)->getNumber() +
                                                lists[modify].getNode(num2 - 1)->getNumber();
                            lists[modify].addLast(tmp.getReal(), tmp.getImaginary());
                            break;
                        }
                        case 'w': {
                            cout << lists[modify] << endl;
                            cout
                                    << "Enter the numbers of elements you would like to sum. They will be summed in the entered order."
                                    << endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            lists[modify].getNode(num1 - 1)->getNumber() += lists[modify].getNode(
                                    num2 - 1)->getNumber();
                            break;
                        }
                        default:
                            cout << "Invalid input.  Try again: " << endl;
                            break;
                    }
                }
                break;
            }
            case '-': {
                unsigned int size = lists[modify].size();
                if (size < 2) {
                    cout << "You need at least 2 elements to subtract, but you have now only " << size
                         << endl;
                } else {
                    cout << "How would you like to subtract elements?" << endl;
                    cout << "n -> as a new element\nw -> subtract the second element from the first one"
                         << endl;
                    char subtract;
                    subtract = secureInput(subtract);
                    switch (subtract) {
                        case 'n': {
                            cout << lists[modify] << endl;
                            cout
                                    << "Enter the numbers of elements you would like to subtract. They will be subtracted in the entered order."
                                    << endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            ComplexNumber tmp = lists[modify].getNode(num1 - 1)->getNumber() -
                                                lists[modify].getNode(num2 - 1)->getNumber();
                            lists[modify].addLast(tmp.getReal(), tmp.getImaginary());
                            break;
                        }
                        case 'w': {
                            cout << lists[modify] << endl;
                            cout
                                    << "Enter the numbers of elements you would like to subtract. They will be subtracted in the entered order."
                                    << endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > size) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            lists[modify].getNode(num1 - 1)->getNumber() -= lists[modify].getNode(
                                    num2 - 1)->getNumber();
                            break;
                        }
                        default:
                            cout << "Invalid input.  Try again: " << endl;
                            break;
                    }
                }
                break;
            }
            case 'c': {
                unsigned int size = lists[modify].size();
                cout << lists[modify] << endl;
                cout << "Enter the numbers of elements you would like to compare." << endl;
                unsigned int num1, num2;
                while (!(cin >> num1) || num1 > size) {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout << "Invalid input.  Try again: " << endl;
                }
                while (!(cin >> num2) || num2 > size) {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                    cout << "Invalid input.  Try again: " << endl;
                }
                if (lists[modify].getNode(num1 - 1)->getNumber() ==
                    lists[modify].getNode(num2 - 1)->getNumber())
                    cout << "These numbers are equal." << endl;
                else
                    cout << "These numbers are not equal." << endl;
                break;
            }
            case 'b': {
                loop = false;
                cout << "Going back to previous menu..." << endl;
                break;
            }
            default:
                cout << "Invalid input.  Try again: " << endl;
                break;
        }
    }
}