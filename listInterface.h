//
// Created by Maciej Dmowski on 2019-03-21.
//

#ifndef PROJECT2_LISTINTERFACE_H
#define PROJECT2_LISTINTERFACE_H

#include <iostream>
#include <vector>
#include <limits>
#include "ComplexNumber.h"
#include "Node.h"
#include "List.h"
#include "secureInput.h"

void listInterface(vector<List>&);

#endif //PROJECT2_LISTINTERFACE_H
