//
// Created by Maciej Dmowski on 2019-03-21.
//
#include "mainInterface.h"

using namespace std;

void displayLists(const vector<List> &v)
{
    cout<<"Existing lists:"<<endl;
    for(unsigned int i=0; i<v.size(); ++i)
    {
        cout<<i+1<<". "<<v[i].name<<endl;
    }
}

void mainInterface(vector<List> &lists) {
    bool repeat = true;
    while (repeat) {
        if (lists.empty()) {
            cout << "No lists. Adding list..." << endl;
            cout << "Enter list name: " << endl;
            string name;
            name = secureInput<string>(name);
            List l1(name);
            lists.push_back(l1);
        }
        cout << endl << "You have now: " << lists.size() << " lists" << endl;
        cout << "Choose what to do:" << endl;
        cout<< "a -> add a list\nd -> delete a list\ns -> show existing lists\n+ -> sum lists\nm -> modify a list\nq -> quit the program"<< endl;

        char choice;
        choice = secureInput(choice);

        switch (choice) {
            case 'a': {
                string name;
                cout << "Enter the list name: " << endl;
                name = secureInput(name);
                List temp(name);
                lists.push_back(temp);
                break;
            }
            case 'd': {
                if (lists.empty())
                    cout << "No lists, add a list first" << endl;
                else {
                    displayLists(lists);
                    cout << "Which one would you like to delete? Enter number from 1 to " << lists.size() << endl;
                    unsigned int del = 0;
                    while (!(cin >> del) || del > lists.size()) {
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        cout << "Invalid input.  Try again: " << endl;
                    }
                    lists[del - 1].deleteList();
                    lists.erase(lists.begin() + del - 1);
                }
                break;
            }
            case 's': {
                char display;
                cout << "Which version would you like to see:\nf->full\ns->short" << endl;
                display = secureInput(display);
                switch (display) {
                    case 'f': {
                        for (unsigned int i = 0; i < lists.size(); ++i) {
                            cout << i + 1 << ".\n" << lists[i];
                            cout << endl;
                        }
                        break;
                    }
                    case 's': {
                        displayLists(lists);
                        break;
                    }
                    default:
                        cout << "Invalid input.  Try again: " << endl;
                        break;
                }
                break;
            }
            case '+': {
                if (lists.size() < 2) {
                    cout << "You need at least 2 lists to sum, but you have now only " << lists.size() << endl;
                } else {
                    cout << "How would you like to sum lists?" << endl;
                    cout << "n -> as a new list\nw -> sum the first list with the second one" << endl;
                    char sum;
                    sum = secureInput(sum);
                    switch (sum) {
                        case 'n': {
                            displayLists(lists);
                            cout
                                    << "Enter the numbers of lists you would like to sum. They will be summed in the entered order."
                                    << endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > lists.size()) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > lists.size()) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            List sumList = lists[num1 - 1] + lists[num2 - 1];
                            lists.push_back(sumList);
                            break;
                        }
                        case 'w': {
                            displayLists(lists);
                            cout<< "Enter the numbers of lists you would like to sum. They will be summed in the entered order."<< endl;
                            unsigned int num1, num2;
                            while (!(cin >> num1) || num1 > lists.size()) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            while (!(cin >> num2) || num2 > lists.size()) {
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << "Invalid input.  Try again: " << endl;
                            }
                            lists[num1 - 1] += lists[num2 - 1];
                            break;
                        }
                        default:
                            cout << "Invalid input.  Try again: " << endl;
                            break;
                    }
                }
                break;
            }
            case 'm': {
                displayLists(lists);
                listInterface(lists);
                break;
            }
            case 'q': {
                repeat = false;
                for(unsigned int i = 0; i < lists.size(); ++i) {
                    lists[i].deleteList();
                }
                cout << "Bye";
                break;
            }
            default:
                cout << "Invalid input.  Try again: " << endl;
                break;
        }
    }
}