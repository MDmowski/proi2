project2.o: main.o ComplexNumber.o Node.o List.o mainInterface.o listInterface.o
	g++ main.o ComplexNumber.o Node.o List.o mainInterface.o listInterface.o -o project2.o

main.o: main.cpp ComplexNumber.h Node.h List.h
	g++ -c -Wall -pedantic -std=c++11 main.cpp

ComplexNumber.o: ComplexNumber.cpp ComplexNumber.h
	g++ -c -Wall -pedantic -std=c++11 ComplexNumber.cpp

Node.o: Node.cpp Node.h
	g++ -c -Wall -pedantic -std=c++11 Node.cpp

List.o: List.cpp List.h
	g++ -c -Wall -pedantic -std=c++11 List.cpp

mainInterface.o: mainInterface.cpp mainInterface.h secureInput.h
	g++ -c -Wall -pedantic -std=c++11 mainInterface.cpp

listInterface.o: listInterface.cpp listInterface.h secureInput.h
	g++ -c -Wall -pedantic -std=c++11 listInterface.cpp

clean:
	rm *.o

debug:
	g++ -g *.cpp -o debug.o
