# Doubly linked list of complex numbers
Second PROI project - self implemented doubly linked list of complex numbers using classes and operators overload. 

### Features:
1. ###### Operations on the lists
    1. Creating a new list
    2. Deleting a list
    3. Merging lists - one after another
        1. as a new list
        2. modifying an existing list
    4. Displaying all lists
        1. extended - list name, number of elements, content
        2. short - list name 
2. ###### Operations on a single list
     1. Adding a new node
        1. to the head
        2. to the tail
     2. Deleting a node
     3. Displaying a list: << operator overload
     4. Summing and subtracting node values - complex numbers
        1. as a new node with summed value
        2. modifying an existing node
     5. Comparing node values - complex numbers
     6. Accessing nth node - [] operator overload
3. ###### Operations on the complex numbers
    1. Summing: + and += operators overload
    2. Subtraction: - and -= operators overload
    3. Comparison: == and != operators overload
    4. Displaying a complex number: << operator overload