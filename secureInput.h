//
// Created by Maciej Dmowski on 2019-03-21.
//

#ifndef PROJECT2_SECUREINPUT_H
#define PROJECT2_SECUREINPUT_H

#include <iostream>
using namespace std;

template<typename T>
T secureInput(T);

template<typename T>
T secureInput(T input) {
    while (!(cin >> input)) {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cout << "Invalid input.  Try again: ";
    }
    return input;
}

#endif //PROJECT2_SECUREINPUT_H
